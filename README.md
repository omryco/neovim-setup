# [Neovim](https://neovim.io/) Setup

- Install [Neovim](https://neovim.io/).
- Run `pip install neovim` to include vim-python support. This is used by the snippets plugin.
- Install [vim-plug](https://github.com/junegunn/vim-plug) as [instructed](https://github.com/junegunn/vim-plug#windows-powershell-1).
- Copy the content of [`sysinit.vim`](sysinit.vim) to your own `sysinit.vim`.
    - Path can be found be opening Neovim and typing `:version`. On Windows, it is usually `<Extracted-Neovim-Folder>\share\nvim\sysinit.vim`.
    - I prefer to clone this repository, and set `sysinit.vim` to be a symlink to the repository's [`sysinit.vim`](sysinit.vim).
- Open Neovim and type `:PlugInstall`. This command installs the plugins specified in [`sysinit.vim`](sysinit.vim), and specifically it clones my [`vim-snippets`](https://gitlab.com/omrycohen/vim-snippets) repository.

Enjoy!
